﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreationEnemys : MonoBehaviour
{
    public GameObject zona1;
    public GameObject zona2;
    public GameObject zona3;
    public GameObject zona4;

    public GameObject enemy1;
    public GameObject enemy2;
    public GameObject enemy3;
    public GameObject enemy4;
    public GameObject enemy5;
    public GameObject enemy6;
    public GameObject enemy7;
    public GameObject enemy8;
    public GameObject enemy9;
    public GameObject enemy10;

    void OnTriggerEnter2D(Collider2D otherCol)
    {
        Debug.Log(otherCol.name + " has entered the trigger zone of " + this.name);

        if (otherCol.name == "z1")
        {
            zona1.SetActive(false);
            enemy1.SetActive(true);
            enemy2.SetActive(true);
        }

        if (otherCol.name == "z2")
        {
            zona2.SetActive(false);
            enemy3.SetActive(true);
            enemy4.SetActive(true);
        }

        if (otherCol.name == "z3")
        {
            zona3.SetActive(false);
            enemy5.SetActive(true);
            enemy6.SetActive(true);
        }

        if (otherCol.name == "z4")
        {
            zona4.SetActive(false);
            enemy7.SetActive(true);
            enemy8.SetActive(true);
            enemy9.SetActive(true);
            enemy10.SetActive(true);
        }
    }
}
